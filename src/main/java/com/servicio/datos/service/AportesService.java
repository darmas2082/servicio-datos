package com.servicio.datos.service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.TransferenciaVO;

public interface AportesService {
	public double AporteDeudaObtener(String codigo) throws Exception;
	public TransferenciaVO PagoAportes(TransferenciaVO transferencia) throws DAOException;
}
