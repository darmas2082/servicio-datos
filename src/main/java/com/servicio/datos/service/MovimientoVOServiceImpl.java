package com.servicio.datos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.MovimientoVO;
import com.servicio.datos.repository.MovimientoVORepository;

@Service
public class MovimientoVOServiceImpl implements MovimientoVOService{
	
	@Autowired
	private MovimientoVORepository movimientoVORepository;
	
	public List<MovimientoVO> ListarMovAhorros(String idtercero,String id_ahorro) throws DAOException{
		return movimientoVORepository.ListarMovAhorros(idtercero, id_ahorro);
	}
}
