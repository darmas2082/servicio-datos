package com.servicio.datos.service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.TransferenciaVO;

public interface TransferenciaService {
	public TransferenciaVO TransferenciaEntreCuentas(TransferenciaVO transferencia) throws DAOException;
	public TransferenciaVO DatosTitular(TransferenciaVO transferencia) throws DAOException;
}
