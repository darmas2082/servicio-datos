package com.servicio.datos.service;

import com.servicio.datos.exception.DAOException;

public interface TokenService {

	public boolean buscarTokenValido(String token) throws DAOException;
	
	public void registrarToken(String token) throws DAOException;
}
