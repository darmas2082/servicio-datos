package com.servicio.datos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.PrestamosVO;
import com.servicio.datos.model.TransferenciaVO;
import com.servicio.datos.repository.FPSRepository;

@Service
public class FPSServiceImpl implements FPSService {

	@Autowired
	private FPSRepository fpsRepository;
	
	public double FPSDeudaObtener(String codigo) throws Exception{
		return fpsRepository.FPSDeudaObtener(codigo);
	}
	public TransferenciaVO PagoFPS(TransferenciaVO transferencia) throws DAOException{
		return fpsRepository.PagoFPS(transferencia);
	}
}
