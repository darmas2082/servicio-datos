package com.servicio.datos.service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.TransferenciaVO;

public interface FPSService {

	public double FPSDeudaObtener(String codigo) throws Exception;
	public TransferenciaVO PagoFPS(TransferenciaVO transferencia) throws DAOException;
}
