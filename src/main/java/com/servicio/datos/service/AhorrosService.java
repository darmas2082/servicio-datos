package com.servicio.datos.service;

import java.util.List;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.AhorrosVO;

public interface AhorrosService {
	public List<AhorrosVO> ListarCtas(String codigo) throws DAOException;
}
