package com.servicio.datos.service;

import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.AhorrosVO;
import com.servicio.datos.repository.AhorrosRepository;

@Service
public class AhorrosServiceImpl implements AhorrosService{

	@Autowired
	private AhorrosRepository ahorrosRepository;
	
	public List<AhorrosVO> ListarCtas(String codigo) throws DAOException{
		List<AhorrosVO> listaAhorros= ahorrosRepository.ListarCtas(codigo);
		List<AhorrosVO> listaAhorrosDestino=  ahorrosRepository.ListarCtasDestino(codigo);
		List<AhorrosVO> listaAhorrosOrigen= ahorrosRepository.ListarCtasOrigen(codigo);
		for (AhorrosVO ahorrosVO : listaAhorros) {
			for (AhorrosVO ahorrosVO2 : listaAhorrosDestino) {
				if(ahorrosVO.getIdAhorro().equals(ahorrosVO2.getIdAhorro())) {
					ahorrosVO.setEsDestino(true);
				}
			}
			for (AhorrosVO ahorrosVO3 : listaAhorrosOrigen) {
				if(ahorrosVO.getIdAhorro().equals(ahorrosVO3.getIdAhorro())) {
					ahorrosVO.setEsOrigen(true);
				}
			}
		}
		
		return listaAhorros;
	}
}
