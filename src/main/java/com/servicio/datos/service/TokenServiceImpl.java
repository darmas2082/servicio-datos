package com.servicio.datos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.repository.TokenRepository;

@Service
public class TokenServiceImpl implements TokenService{
	
	@Autowired
	private TokenRepository tokenRepository;
	
	public boolean buscarTokenValido(String token) throws DAOException {
		return tokenRepository.buscarTokenValido(token);
	}
	
	public void registrarToken(String token) throws DAOException{
		tokenRepository.registrarToken(token);
	}
}
