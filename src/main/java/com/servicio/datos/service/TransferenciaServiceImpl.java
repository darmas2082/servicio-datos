package com.servicio.datos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.TransferenciaVO;
import com.servicio.datos.repository.TransferenciaRepository;

@Service
public class TransferenciaServiceImpl implements TransferenciaService {

	@Autowired
	private TransferenciaRepository transferenciaRepository;
	
	public TransferenciaVO TransferenciaEntreCuentas(TransferenciaVO transferencia) throws DAOException{
		return transferenciaRepository.TransferenciaEntreCuentas(transferencia);
	}
	
	public TransferenciaVO DatosTitular(TransferenciaVO transferencia) throws DAOException{
		return transferenciaRepository.DatosTitular(transferencia);
	}
}
