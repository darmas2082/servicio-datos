package com.servicio.datos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.exception.LoginException;
import com.servicio.datos.exception.ServiceException;
import com.servicio.datos.model.SocioVO;
import com.servicio.datos.repository.SocioRepository;
@Service
public class SocioServiceImpl implements SocioService{
	
	@Autowired
	private SocioRepository socioRepository;
	
	public SocioVO validarClaveMobile(String nroTarjeta,String pin) throws DAOException{
		return socioRepository.validarTarjeta(nroTarjeta, pin);
	}
	
	public SocioVO registrarClaveWeb(String nroTarjeta, String pin) throws ServiceException{
		SocioVO vo = new SocioVO();
		try{
			
			vo = socioRepository.registrarClaveWeb(nroTarjeta, pin);
		
			}catch(DAOException e){
			
			throw new ServiceException(e);
			
		}
		return vo;

	}
	
	public SocioVO validarPIN(String nroTarjeta,Integer pin) throws ServiceException{
		SocioVO vo = new SocioVO();
		
		try{
			
		vo = socioRepository.validarPIN(nroTarjeta, pin);
		
		
		}catch(LoginException e){
			
			throw new ServiceException(e);
			
		}
		return vo;

	}
}
