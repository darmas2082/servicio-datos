package com.servicio.datos.service;

import java.util.List;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.MovimientoVO;

public interface MovimientoVOService {

	public List<MovimientoVO> ListarMovAhorros(String idtercero,String id_ahorro) throws DAOException;
}
