package com.servicio.datos.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.AportesVO;
import com.servicio.datos.model.TransferenciaVO;
import com.servicio.datos.repository.AportesRepository;

@Service
public class AportesServiceImpl implements AportesService{
	
	@Autowired
	private AportesRepository aportesRepository;
	
	public double AporteDeudaObtener(String codigo) throws Exception{
		return aportesRepository.AporteDeudaObtener(codigo);
	}
	public TransferenciaVO PagoAportes(TransferenciaVO transferencia) throws DAOException{
		return aportesRepository.PagoAportes(transferencia);
	}
}
