package com.servicio.datos.service;

import java.util.List;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.MovimientoPrestamoVO;
import com.servicio.datos.model.PrestamosVO;
import com.servicio.datos.model.TransferenciaVO;

public interface PrestamosService {
	
	public List<PrestamosVO> ListarPrestamos(String Codigo) throws DAOException;
	public List<MovimientoPrestamoVO> ListarCuotasPendientes(String idPrestamo)throws DAOException;
	public List<MovimientoPrestamoVO> ListarCuotasPagadas(String idPrestamo)throws DAOException;
	public List<PrestamosVO>  ListaPrestamosDestino(PrestamosVO prestamo) throws DAOException;
	public TransferenciaVO PagoPrestamo(TransferenciaVO transferencia) throws DAOException;
}
