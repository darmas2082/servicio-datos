package com.servicio.datos.service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.exception.ServiceException;
import com.servicio.datos.model.SocioVO;

public interface SocioService {
	public SocioVO validarClaveMobile(String nroTarjeta,String pin) throws DAOException;
	
	
	public SocioVO validarPIN(String nroTarjeta,Integer pin) throws ServiceException;
	
	public SocioVO registrarClaveWeb(String nroTarjeta,String pin) throws ServiceException;
}
