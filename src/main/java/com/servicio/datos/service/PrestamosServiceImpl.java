package com.servicio.datos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.MovimientoPrestamoVO;
import com.servicio.datos.model.PrestamosVO;
import com.servicio.datos.model.TransferenciaVO;
import com.servicio.datos.repository.PrestamosRepository;

@Service
public class PrestamosServiceImpl implements PrestamosService {
	
	@Autowired
	private PrestamosRepository prestamosRepository;
	
	public List<PrestamosVO>  ListaPrestamosDestino(PrestamosVO prestamo) throws DAOException {
		return prestamosRepository.ListaPrestamosDestino(prestamo);
	}
	
	public TransferenciaVO PagoPrestamo(TransferenciaVO transferencia) throws DAOException{
		return prestamosRepository.PagoPrestamo(transferencia);
	}
	
	public List<PrestamosVO> ListarPrestamos(String Codigo) throws DAOException{
		return prestamosRepository.ListarPrestamos(Codigo);
	}
	
	
	public List<MovimientoPrestamoVO> ListarCuotasPendientes(String idPrestamo)throws DAOException{
		return prestamosRepository.ListarCuotasPendientes(idPrestamo);
	}
	public List<MovimientoPrestamoVO> ListarCuotasPagadas(String idPrestamo)throws DAOException{
		return prestamosRepository.ListarCuotasPagadas(idPrestamo);
	}

}
