package com.servicio.datos.service;

import java.util.List;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.DocumentosVO;
import com.servicio.datos.model.Prestamos;

public interface DocumentoService {

	public List<Prestamos> ListarDocumentos(String Codigo) throws DAOException;
}
