package com.servicio.datos.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.DocumentosVO;
import com.servicio.datos.model.Prestamos;
import com.servicio.datos.repository.DocumentoRepository;

@Service
public class DocumentoServiceImpl implements DocumentoService {

	@Autowired
	private DocumentoRepository documentoRepository;
	
	public List<Prestamos> ListarDocumentos(String Codigo) throws DAOException{
		return documentoRepository.ListarDocumentos(Codigo);
	}
}
