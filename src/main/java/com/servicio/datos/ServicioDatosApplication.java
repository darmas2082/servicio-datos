package com.servicio.datos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServicioDatosApplication {

	public static void main(String[] args) {
		System.setProperty("java.security.egd", "file:///dev/urandom");
		SpringApplication.run(ServicioDatosApplication.class, args);
	}

}
