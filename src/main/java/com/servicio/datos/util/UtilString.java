package com.servicio.datos.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

import org.apache.commons.codec.binary.Hex;

public class UtilString {
	/**
	 * Completa N ceros adelante para un número de tipo INTEGER
	 * @param cantidad
	 * @param numero
	 * @return
	 */
	public static String completaCeros(int cantidad, Integer numero) {
		StringBuilder sb = new StringBuilder();
		sb.append("%0");
		sb.append(cantidad);
		sb.append("d");
		
		return String.format(sb.toString(), numero);
	}
	
	/**
	 * Completa N ceros adelante para un número de tipo LONG
	 * @param cantidad
	 * @param numero
	 * @return
	 */
	public static String completaCeros(int cantidad, Long numero) {
		StringBuilder sb = new StringBuilder();
		sb.append("%0");
		sb.append(cantidad);
		sb.append("d");
		
		return String.format(sb.toString(), numero);
	}
	
	/**
	 * Extrae N caracteres de un numero
	 * @param cadena
	 * @param ini
	 * @param fin
	 * @return
	 */
	public static String substring(Integer cadena, int ini, int fin) {
		return cadena.toString().substring(ini, fin);
	}
	
	/**
	 * Genera una cadena aleatoria alphanumerica de N caracteres
	 * @param cantidad
	 * @return 
	 */
	public static String aleatorio(int cantidad) {
		int leftLimit = 48;
	    int rightLimit = 122;
	    int targetStringLength = cantidad;
	    Random random = new Random();
	 
	    String generatedString = random.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57) || (i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();
	    
		return generatedString;
	}
	
	/**
	 * Encripta una cadena en Hash SHA-1
	 * @param cadena
	 * @return
	 */
	public static String encriptaCadena(String cadena) {
		String respuesta;
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-1");
	        md.update(cadena.getBytes());
	        byte[] mb = md.digest();
	        
	        respuesta = new String(Hex.encodeHex(mb));
		} catch (NoSuchAlgorithmException e) {
			respuesta = "";
		}
        
        return respuesta;
	}
	
	public static String fechaActual() {
		return formatoFecha("yyyy-MM-dd");
	}
	
	public static String fecha(String formato) {
		return formatoFecha(formato);
	}
	
	public static String horaActual() {
		return formatoFecha("HH:mm:ss");
	}
	
	public static String hora(String formato) {
		return formatoFecha(formato);
	}
	
	private static String formatoFecha(String formato) {
		DateTimeFormatter formateador = DateTimeFormatter.ofPattern(formato);
		LocalDateTime ahora = LocalDateTime.now();
		
		return formateador.format(ahora);
	}
}
