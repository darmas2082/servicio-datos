package com.servicio.datos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.exception.ServiceException;
import com.servicio.datos.model.PinResponse;
import com.servicio.datos.model.SocioVO;
import com.servicio.datos.model.listaAhorrosResponse;
import com.servicio.datos.service.SocioService;

@RestController
public class PinController {

	@Autowired
	SocioService socioService;
	
	private boolean esNumero(String cadena){
		   
	      try {
	   
	      Integer.parseInt(cadena);
	   
	      return true;
	   
	      } catch (NumberFormatException nfe){
	   
	      return false;
	   
	      }     
	}
	
	@PostMapping("/validaPin")
	public PinResponse obtenerAhorro(@RequestBody SocioVO socioVO, Object session ) throws DAOException{
		PinResponse pinR=new PinResponse();
		try {
			if (socioVO.getPin().length() != 4 || socioVO.getNroTarjeta().length() !=16){
				pinR.setMensaje("Nro. Tarjeta / Clave incorrectos");
				pinR.setSuccess(false);
				return pinR;
			}
			if (!esNumero(socioVO.getPin())){
				pinR.setMensaje("Ingrese una clave valida");
				pinR.setSuccess(false);
				return pinR;
			}
			
			try {
				
				int pin = Integer.parseInt(socioVO.getPin());
				
				SocioVO vo = socioService.validarPIN(socioVO.getNroTarjeta(),pin);
				
				if(vo.equals(null)){
					pinR.setMensaje("Nro. tarjeta / Clave incorrectos.");
					pinR.setSuccess(false);
					return pinR;
				}
				if(!vo.getSuccess()) {
					pinR.setSuccess(false);
					pinR.setMensaje(vo.getMensajeDetallado());
					return pinR;
				}
				pinR.setSuccess(true);
				pinR.setMensaje("Nro. tarjeta y clave correctas");
				return pinR;
			}catch (ServiceException e) {
				pinR.setMensaje("Nro. tarjeta / Clave incorrectos.");
				pinR.setSuccess(false);
				return pinR;
				
			}
				
		}catch(Exception ex) {
			pinR.setMensaje("Error en Servicio." + ex.getMessage());
			pinR.setSuccess(false);
			return pinR;
		}
		
	}
	
	
	
}
