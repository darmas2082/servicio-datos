package com.servicio.datos.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.servicio.datos.model.SecureToken;
import com.servicio.datos.model.SocioVO;
import com.servicio.datos.service.SocioService;
import com.servicio.datos.service.TokenService;

@RestController
public class LoginController {

	@Autowired
	private SocioService socioService;
	
	@Autowired
	private TokenService tokenService;
	
	
	@GetMapping("/login/{nroTarjeta}/{pin}/{token}/{callback}")
	public Map<String, Object> obtener(@PathVariable("nroTarjeta") String nroTarjeta,
			@PathVariable("pin") String pin,@PathVariable("token") String token,
			@PathVariable("callback") String callback){
		
		Map<String, Object> respuesta=new HashMap<>();
		try {
			SocioVO data=socioService.validarClaveMobile(nroTarjeta, pin);
			respuesta.put("data", data);
			respuesta.put("success", true);
		}catch(Exception ex) {
			respuesta.put("message", ex.getMessage());
			respuesta.put("success", false);
		}
		return respuesta;
	}
	
	@PostMapping("/login")
	public SocioVO obtenerLogin(@RequestBody SocioVO socioVO){
		
		SocioVO vo = new SocioVO();
		vo = new SocioVO();
	
		
		if(socioVO.getNroTarjeta() == null || socioVO.getPin() == null || socioVO.getNroTarjeta().length() != 16){
			
			vo.setCodigo("JSR-101");
			vo.setMensaje("Error en parametros");
			vo.setSuccess(false);
			return vo;
		}
		
		
		try{
			
			vo = socioService.validarClaveMobile(socioVO.getNroTarjeta(), socioVO.getPin());
		
			}catch (Exception e){
			
				vo.setCodigo("JSR-201");
				vo.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
				vo.setSuccess(false);
			
			
			return vo;
			
			}
		
		if(vo != null){
			
			SecureToken st = new SecureToken();
			String stoken = st.generateToken(vo.getCodigo());
			
			try{
				
				tokenService.registrarToken(stoken);
			
			}catch (Exception e){
				vo.setCodigo("JSR-201");
				vo.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
				vo.setSuccess(false);
				return vo;
			}
			vo.setToken(stoken);
			vo.setSuccess(true);
			return vo;
			
		} else {
			vo=new SocioVO();
			vo.setCodigo("JSR-103");
			vo.setMensaje("Tarjeta o Clave incorrectos");
			vo.setSuccess(false);
			
			
			return vo;
		}
		
		
		
	}
}
