package com.servicio.datos.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.MovimientoPrestamoVO;
import com.servicio.datos.model.PinResponse;
import com.servicio.datos.model.PrestamosVO;
import com.servicio.datos.model.SocioVO;
import com.servicio.datos.model.TransferenciaVO;
import com.servicio.datos.model.listaPrestamosResponse;
import com.servicio.datos.service.PrestamosService;
import com.servicio.datos.service.TokenService;

@RestController
public class PrestamosController {

	@Autowired
	private PrestamosService prestamosService;
	
	@Autowired
	private TokenService tokenService;
	
	
	
	@PostMapping("/prestamos")
	public listaPrestamosResponse obtenerPrestamos(@RequestBody SocioVO socioVO) throws DAOException{
		
		listaPrestamosResponse prestamos=new listaPrestamosResponse();
		try {
			boolean esValido = false;
			if(socioVO.getCodigo() == null || socioVO.getToken() == null ){
				prestamos.setCodigo("JSR-101");
				prestamos.setMensaje("Parametros incorrectos");
				prestamos.setSuccess(false);
				return prestamos;
			}

			try{
				esValido = tokenService.buscarTokenValido(socioVO.getToken());
				
			}catch (Exception e){
				
				prestamos.setCodigo("JSR-201");
				prestamos.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
				prestamos.setSuccess(false);
				return prestamos;
			}

			if(esValido == true){
				prestamos.setPrestamos(prestamosService.ListarPrestamos(socioVO.getCodigo()));
				if(prestamos.getPrestamos()==null || prestamos.getPrestamos().size()==0) {
					prestamos.setCodigo("Msg-105");
					prestamos.setMensaje("No cuenta con prestamos");
					prestamos.setSuccess(false);
					return prestamos;
				}else {
					prestamos.setSuccess(true);
					return prestamos;
				}
			} else {
				prestamos.setCodigo("JSR-105");
				prestamos.setMensaje("No Autenticado o Session Expirada");
				prestamos.setSuccess(false);
				return prestamos;
			}
		}catch(Exception ex) {
			prestamos.setCodigo("JSR-201");
			prestamos.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
			prestamos.setSuccess(false);
			return prestamos;
		}
		
	}
	
	@PostMapping("/movimientoPrestamoPendiente")
	public PrestamosVO obtenerMovimientoPrestamos(@RequestBody PrestamosVO prestamosVO) throws DAOException{
		
		boolean esValido = false;
		
		if(prestamosVO.getId_prestamo() == null || prestamosVO.getToken() == null || prestamosVO.getToken().length() != 76){
			
			prestamosVO.setMensaje("Parametros incorrectos");
			prestamosVO.setCodigo("JSR-101");
			prestamosVO.setSuccess(false);
			return prestamosVO;
			
		}
		
		

		try{
			esValido = tokenService.buscarTokenValido(prestamosVO.getToken());
			
		}catch (Exception e){
			prestamosVO.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
			prestamosVO.setCodigo("JSR-201");
			prestamosVO.setSuccess(false);
			return prestamosVO;
		}
		
		if(esValido == true){

				List<MovimientoPrestamoVO> movimientos = new ArrayList<MovimientoPrestamoVO>();
				
				movimientos = prestamosService.ListarCuotasPendientes(prestamosVO.getId_prestamo());
				if(!movimientos.isEmpty()){
					prestamosVO.setCuotasPendiente(movimientos);
				}else{
					
					prestamosVO.setMensaje("No existen pagos para este prestamo");
					prestamosVO.setCodigo("Msg-104");
					prestamosVO.setSuccess(false);
					return prestamosVO;
				}
				
				
				
			} else {
				
				prestamosVO.setMensaje("No Autenticado o Session Expirada");
				prestamosVO.setCodigo("JSR-105");
				prestamosVO.setSuccess(false);
				return prestamosVO;
			}
		return prestamosVO;
		
	}
	
	
	@PostMapping("/movimientoPrestamo")
	public PrestamosVO obtenerMovimientoPrestamosPagos(@RequestBody PrestamosVO prestamosVO) throws DAOException{
		
		boolean esValido = false;
		
		if(prestamosVO.getId_prestamo() == null || prestamosVO.getToken() == null || prestamosVO.getToken().length() != 76){
			
			prestamosVO.setMensaje("Parametros incorrectos");
			prestamosVO.setCodigo("JSR-101");
			prestamosVO.setSuccess(false);
			return prestamosVO;
			
		}
		
		

		try{
			esValido = tokenService.buscarTokenValido(prestamosVO.getToken());
			
		}catch (Exception e){
			prestamosVO.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
			prestamosVO.setCodigo("JSR-201");
			prestamosVO.setSuccess(false);
			return prestamosVO;
		}
		
		if(esValido == true){

				List<MovimientoPrestamoVO> movimientosPagado = new ArrayList<MovimientoPrestamoVO>();
				List<MovimientoPrestamoVO> movimientosPendiente = new ArrayList<MovimientoPrestamoVO>();
				movimientosPagado = prestamosService.ListarCuotasPagadas(prestamosVO.getId_prestamo());
				movimientosPendiente = prestamosService.ListarCuotasPendientes(prestamosVO.getId_prestamo());
				if(!movimientosPagado.isEmpty()){
					prestamosVO.setCuotasPagadas(movimientosPagado);
				}else{
					
					prestamosVO.setMensaje("No existen pagos para este prestamo.");
					prestamosVO.setCodigo("Msg-104");
				}
				if(!movimientosPendiente.isEmpty()){
					prestamosVO.setCuotasPendiente(movimientosPendiente);
				}else{
					
					prestamosVO.setMensaje(prestamosVO.getMensaje() +  " No existen cuotas pendientes para este prestamo");
					prestamosVO.setCodigo("Msg-104");
				}
			} else {
				
				prestamosVO.setMensaje("No Autenticado o Session Expirada");
				prestamosVO.setCodigo("JSR-105");
				prestamosVO.setSuccess(false);
				return prestamosVO;
			}
		prestamosVO.setSuccess(true);
		return prestamosVO;
		
	}
	@PostMapping("/prestamoPendiente")
	public listaPrestamosResponse obtenerPrestamosPendiente(@RequestBody PrestamosVO prestamosVO) throws DAOException{
		
		boolean esValido = false;
		listaPrestamosResponse prestamosR=new listaPrestamosResponse();
		if(prestamosVO.getCodigo() == null || prestamosVO.getToken() == null || prestamosVO.getToken().length() != 76){
			
			prestamosR.setMensaje("Parametros incorrectos");
			prestamosR.setCodigo("JSR-101");
			prestamosR.setSuccess(false);
			return prestamosR;
			
		}
		try{
			esValido = tokenService.buscarTokenValido(prestamosVO.getToken());
			
		}catch (Exception e){
			prestamosR.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
			prestamosR.setCodigo("JSR-201");
			prestamosR.setSuccess(false);
			return prestamosR;
		}
		
		if(esValido == true){
			List<PrestamosVO> listaPrestamos=prestamosService.ListaPrestamosDestino(prestamosVO);
				if(listaPrestamos.isEmpty()){
					prestamosR.setMensaje("No existen préstamos.");
					prestamosR.setCodigo("Msg-104");
				}else {
					prestamosR.setPrestamos(listaPrestamos);
				}
				
			} else {
				
				prestamosR.setMensaje("No Autenticado o Session Expirada");
				prestamosR.setCodigo("JSR-105");
				prestamosR.setSuccess(false);
				return prestamosR;
			}
		prestamosR.setSuccess(true);
		return prestamosR;
		
	}
	
	@PostMapping("/pagoPrestamo")
	public TransferenciaVO transferenciaPropias(@RequestBody TransferenciaVO transferencia ) throws DAOException{
		TransferenciaVO transferenciaR=new TransferenciaVO();
		boolean esValido = false;
		if(transferencia.getCtaOrigen()== null || transferencia.getToken() == null || transferencia.getPrestamoDestino()==null ){
			transferenciaR.setDetalleRespuesta("Parametros incorrectos");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
		try{
			esValido = tokenService.buscarTokenValido(transferencia.getToken());
		}catch (Exception e){
			transferenciaR.setDetalleRespuesta("Por favor comunicarse con Soporte y notificar codigo de error");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
		
		if(esValido == true){
			
			transferenciaR = prestamosService.PagoPrestamo(transferencia);
			transferenciaR.setSuccess(true);
			return transferenciaR;
		} else {
			transferenciaR.setDetalleRespuesta("No Autenticado");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
	
	}
}
