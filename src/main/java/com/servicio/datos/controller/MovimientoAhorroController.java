package com.servicio.datos.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.servicio.datos.model.MovimientoResponse;
import com.servicio.datos.model.MovimientoVO;
import com.servicio.datos.service.MovimientoVOService;
import com.servicio.datos.service.TokenService;

@RestController
public class MovimientoAhorroController {

	
	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private MovimientoVOService movimientoVOService;
	
	@PostMapping("/movimientosahorros")
	//public Map<String, Object> obtener(@PathVariable("idTercero") String idTercero,
	public MovimientoResponse obtenerPost(@RequestBody MovimientoVO movimiento){
		
		
		MovimientoResponse movimientoR=new MovimientoResponse();
		try {
			
			boolean esValido = false;
			
			if(movimiento.getIdTercero()== null || movimiento.getIdAhorro()== null || movimiento.getToken()== null){
				movimientoR.setCodigo("JSR-101");
				movimientoR.setMensaje("Parametros incorrectos");
				
				
				return movimientoR;
			}
			

			try{
				esValido = tokenService.buscarTokenValido(movimiento.getToken());
				
			}catch (Exception e){
				movimientoR.setCodigo("JSR-201");
				movimientoR.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
				
				
				return movimientoR;
				
			}
			
			if(esValido == true){
				
				List<MovimientoVO> movimientos = new ArrayList<MovimientoVO>();
				
				movimientos=movimientoVOService.ListarMovAhorros(movimiento.getIdTercero(),movimiento.getIdAhorro());
				
				if(!movimientos.isEmpty()){
					
					movimientoR.setAhorros(movimientos);
					movimientoR.setSuccess(true);
				} else {
					
					movimientoR.setCodigo("Msg-102");
					movimientoR.setMensaje("No tiene movimientos");
					movimientoR.setSuccess(false);
					
					return movimientoR;
				}
				return movimientoR;
				
				} else {
					movimientoR.setCodigo("JSR-105");
					movimientoR.setMensaje("No Autenticado o Session Expirada");
					movimientoR.setSuccess(false);
					
					return movimientoR;
				}
			
			
		}catch(Exception ex) {
			movimientoR.setSuccess(false);
			movimientoR.setMensaje(ex.getMessage());
			
		}
		return movimientoR;
	}
}
