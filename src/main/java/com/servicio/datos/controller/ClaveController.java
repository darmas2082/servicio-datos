package com.servicio.datos.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.exception.ServiceException;
import com.servicio.datos.model.PinResponse;
import com.servicio.datos.model.SocioVO;
import com.servicio.datos.service.SocioService;

@RestController
public class ClaveController {

	@Autowired
	SocioService socioService;
	
	private boolean esNumero(String cadena){
		   
	      try {
	   
	      Integer.parseInt(cadena);
	   
	      return true;
	   
	      } catch (NumberFormatException nfe){
	   
	      return false;
	   
	      }     
	}
	private boolean validarClave(String clave){
		
		String[] array = {"123456","654321","111111","222222","333333","444444","555555","666666","777777","888888","999999","000000"};
		
		for(int i = 0; i<array.length; i++){
			
		  if(array[i].equals(clave)){
			  
			 return true;
			 
		  }
		}
		
		return false;
	}
	@PostMapping("/creaPin")
	public PinResponse crearPin(@RequestBody SocioVO socioVO ) throws DAOException{
		PinResponse pinR=new PinResponse();
		try {

			
			

			if (socioVO.getPin().length() != 6 ){
				pinR.setMensaje("Su clave debe tener 6 numeros");
				pinR.setSuccess(false);
				return pinR;
				
			}
			if (!esNumero(socioVO.getPin())){
				pinR.setMensaje("Ingrese una clave valida");
				pinR.setSuccess(false);
				return pinR;
				
				
			}
			
			if (validarClave(socioVO.getPin())){
				
				pinR.setMensaje("Por su seguridad elija otra clave");
				pinR.setSuccess(false);
				return pinR;
				
				
			}
			
			try {
				
				String tarjeta = socioVO.getNroTarjeta();
				socioService.registrarClaveWeb(socioVO.getNroTarjeta(), socioVO.getPin());
				
			}catch(ServiceException e){
				e.printStackTrace();
				pinR.setMensaje("Error en Servicio");
				pinR.setSuccess(false);
				
				return pinR;
				
			}
			
			
			pinR.setMensaje("Ha generado su clave de forma satisfactoria");
			pinR.setSuccess(false);
			
			return pinR;
			
				
		}catch(Exception ex) {
			pinR.setMensaje("Error en Servicio." + ex.getMessage());
			pinR.setSuccess(false);
			return pinR;
		}
		
	}
}
