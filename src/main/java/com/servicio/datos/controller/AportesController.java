package com.servicio.datos.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.servicio.datos.model.SocioVO;
import com.servicio.datos.model.TransferenciaVO;
import com.servicio.datos.service.AportesService;
import com.servicio.datos.service.TokenService;

@RestController
public class AportesController {

	@Autowired
	private AportesService aportesService;
	
	@Autowired
	private TokenService tokenService;
	
	@PostMapping("/deudaAportes")
	public Map<String, Object> obtenerDeuda(@RequestBody SocioVO socio) throws Exception{
		
		Map<String, Object> aporteResponse=new HashMap<>();
		try {
			boolean esValido = false;

			if(socio.getToken() == null || socio.getCodigo().isEmpty() || socio.getToken().isEmpty()){
				aporteResponse.put("codigo", "JSR-101");
				aporteResponse.put("mensaje","Parametros incorrectos");
				aporteResponse.put("success",false);
				return  aporteResponse;
			}
			
			

			try{
				
				esValido = tokenService.buscarTokenValido(socio.getToken());
				
			}catch (Exception e){
				aporteResponse.put("codigo", "JSR-201");
				aporteResponse.put("mensaje","Por favor comunicarse con Soporte y notificar codigo de error");
				aporteResponse.put("success",false);
				return  aporteResponse;
				
			}
			if(esValido == true){
				double monto=aportesService.AporteDeudaObtener(socio.getCodigo());
				aporteResponse.put("deuda", monto);
				aporteResponse.put("success",true);
				return aporteResponse;
				
			} else {
				aporteResponse.put("codigo", "JSR-105");
				aporteResponse.put("mensaje","No Autenticado");
				aporteResponse.put("success",false);
				return  aporteResponse;
				
			}
		}catch(Exception e) {
			aporteResponse.put("codigo","JSR-201");
			aporteResponse.put("mensaje",e.getMessage());
			aporteResponse.put("success",false);
			return  aporteResponse;
		}
		
	}
	
	@PostMapping("/pagoAportes")
	public TransferenciaVO pagoAportes(@RequestBody TransferenciaVO transferencia ) throws Exception{
		TransferenciaVO transferenciaR=new TransferenciaVO();
		boolean esValido = false;
		if(transferencia.getCtaOrigen()== null || transferencia.getToken() == null || transferencia.getCodigo()==null ){
			transferenciaR.setDetalleRespuesta("Parametros incorrectos");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
		try{
			esValido = tokenService.buscarTokenValido(transferencia.getToken());
		}catch (Exception e){
			transferenciaR.setDetalleRespuesta("Por favor comunicarse con Soporte y notificar codigo de error");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
		 
		if(esValido == true){
			
			transferenciaR = aportesService.PagoAportes(transferencia);
			transferenciaR.setSuccess(true);
			return transferenciaR;
		} else {
			transferenciaR.setDetalleRespuesta("No Autenticado");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
	
	}
}
