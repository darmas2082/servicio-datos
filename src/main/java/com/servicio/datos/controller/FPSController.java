package com.servicio.datos.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.DocumentosVO;
import com.servicio.datos.model.Prestamos;
import com.servicio.datos.model.SecureToken;
import com.servicio.datos.model.SocioVO;
import com.servicio.datos.model.TransferenciaVO;
import com.servicio.datos.model.listaFPSResponse;
import com.servicio.datos.service.DocumentoService;
import com.servicio.datos.service.FPSService;
import com.servicio.datos.service.TokenService;

@RestController
public class FPSController {

	@Autowired
	private TokenService tokenService;
	
	@Autowired
	private DocumentoService documentoService;
	
	@Autowired
	private FPSService fpsService;
	
	@PostMapping("/listarfps")
	public listaFPSResponse obtenerPost(@RequestBody Prestamos prestamos) throws DAOException{
		
		listaFPSResponse fpsResponse=new listaFPSResponse();
		boolean esValido = false;

		if(prestamos.getIdTercero() == null || prestamos.getToken() == null || prestamos.getIdTercero().isEmpty() || prestamos.getToken().isEmpty()){
			fpsResponse.setCodigo("JSR-101");
			fpsResponse.setMensaje("Parametros incorrectos");
			fpsResponse.setSuccess(false);
			
			return  fpsResponse;
		}
		
		

		try{
			
			esValido = tokenService.buscarTokenValido(prestamos.getToken());
			
		}catch (Exception e){
			
			fpsResponse.setSuccess(false);
			fpsResponse.setCodigo("JSR-201");
			fpsResponse.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
			return fpsResponse;
		}
		
		SecureToken st = new SecureToken();
		
		if(esValido == true){
			
			if(st.validateToken(prestamos.getIdTercero(), prestamos.getToken())){

			List<Prestamos> pagos = new ArrayList<Prestamos>();
			pagos=documentoService.ListarDocumentos(prestamos.getIdTercero());
			fpsResponse.setPrestamos(pagos);
			fpsResponse.setSuccess(true);
			
			return fpsResponse;
			
			} else {
				
				
				fpsResponse.setCodigo("Msg-101");
				fpsResponse.setMensaje("No cuenta con pagos al Fondo de Prevision Social");
				
				return fpsResponse;
			}
			} else {
				fpsResponse.setSuccess(false);
				fpsResponse.setCodigo( "JSR-105");
				fpsResponse.setMensaje( "No Autenticado");
				
				return fpsResponse;
			}
	}
	
	@PostMapping("/deudaFPS")
	public Map<String, Object> obtenerDeuda(@RequestBody SocioVO socio) throws Exception{
		
		
		Map<String, Object> fpsResponse=new HashMap<>();
		boolean esValido = false;

		if(socio.getToken() == null || socio.getCodigo().isEmpty() || socio.getToken().isEmpty()){
			fpsResponse.put("codigo", "JSR-101");
			fpsResponse.put("mensaje","Parametros incorrectos");
			fpsResponse.put("success",false);
			return  fpsResponse;
		}
		
		

		try{
			
			esValido = tokenService.buscarTokenValido(socio.getToken());
			
		}catch (Exception e){
			fpsResponse.put("codigo", "JSR-201");
			fpsResponse.put("mensaje","Por favor comunicarse con Soporte y notificar codigo de error");
			fpsResponse.put("success",false);
			return  fpsResponse;
			
		}
		if(esValido == true){
			double monto=fpsService.FPSDeudaObtener(socio.getCodigo());
			fpsResponse.put("deuda", monto);
			fpsResponse.put("success",true);
			return fpsResponse;
			
		} else {
			fpsResponse.put("codigo", "JSR-105");
			fpsResponse.put("mensaje","No Autenticado");
			fpsResponse.put("success",false);
			return  fpsResponse;
			
		}
	}
	
	@PostMapping("/pagoFPS")
	public TransferenciaVO pagoFPS(@RequestBody TransferenciaVO transferencia ) throws Exception{
		TransferenciaVO transferenciaR=new TransferenciaVO();
		boolean esValido = false;
		if(transferencia.getCtaOrigen()== null || transferencia.getToken() == null || transferencia.getCodigo()==null ){
			transferenciaR.setDetalleRespuesta("Parametros incorrectos");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
		try{
			esValido = tokenService.buscarTokenValido(transferencia.getToken());
		}catch (Exception e){
			transferenciaR.setDetalleRespuesta("Por favor comunicarse con Soporte y notificar codigo de error");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
		 
		if(esValido == true){
			
			transferenciaR = fpsService.PagoFPS(transferencia);
			transferenciaR.setSuccess(true);
			return transferenciaR;
		} else {
			transferenciaR.setDetalleRespuesta("No Autenticado");
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
	
	}
}
