package com.servicio.datos.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.AhorrosVO;
import com.servicio.datos.model.CuentaMensaje;
import com.servicio.datos.model.Mensaje;
import com.servicio.datos.model.TransferenciaVO;
import com.servicio.datos.model.listaAhorrosResponse;
import com.servicio.datos.service.TokenService;
import com.servicio.datos.service.TransferenciaService;
import com.servicio.datos.util.Util;
import com.servicio.datos.util.UtilString;

@RestController
public class TransferenciaController {

	@Autowired
	TransferenciaService transferenciaService;
	
	@Autowired
	TokenService tokenService;
	
	
	@PostMapping("/transferencia-propia")
	public TransferenciaVO transferenciaPropias(@RequestBody TransferenciaVO transferencia ) throws Exception{
		TransferenciaVO transferenciaR=new TransferenciaVO();
		try {
			
			boolean esValido = false;
			if(transferencia.getCtaOrigen()== null || transferencia.getToken() == null || transferencia.getCtaDestino()==null ){
				transferenciaR.setDetalleRespuesta("Parametros incorrectos");
				transferenciaR.setSuccess(false);
				return transferenciaR;
			}
			try{
				esValido = tokenService.buscarTokenValido(transferencia.getToken());
			}catch (Exception e){
				transferenciaR.setDetalleRespuesta("Por favor comunicarse con Soporte y notificar codigo de error");
				transferenciaR.setSuccess(false);
				return transferenciaR;
			}
			 
			if(esValido == true){
				
				transferenciaR = transferenciaService.TransferenciaEntreCuentas(transferencia);
				if(transferenciaR.getIdTransaccion()!=null) {
					transferenciaR.setSuccess(true);
					EnvioMensaje(transferencia, transferenciaR.getIdTransaccion());	
				}else {
					transferenciaR.setSuccess(false);
				}
				
				return transferenciaR;
			} else {
				transferenciaR.setDetalleRespuesta("No Autenticado");
				transferenciaR.setSuccess(false);
				return transferenciaR;
			}	
		}catch(Exception e)
		{
			transferenciaR.setDetalleRespuesta(e.getMessage());
			transferenciaR.setSuccess(false);
			return transferenciaR;
		}
		
	
	}
	
	
	private void EnvioMensaje(TransferenciaVO transferencia, Integer nroOperacion) throws Exception {
		
		TransferenciaVO datosTitular=transferenciaService.DatosTitular(transferencia);
		
		String correoDestino = datosTitular.getCorreo();
		System.out.println("El correo a enviar:"+correoDestino );
		//correoDestino = "darmas2082@gmail.com";
		
		SimpleDateFormat objSDF = new SimpleDateFormat("dd/mm/yyyy");
		Calendar calendario = new GregorianCalendar();
		int hora, minutos, segundos;
		hora =calendario.get(Calendar.HOUR_OF_DAY);
		minutos = calendario.get(Calendar.MINUTE);
		segundos = calendario.get(Calendar.SECOND);
		String horaSys = hora+":"+minutos+":"+segundos;
		String fechaTxt=objSDF.format(new Date(Calendar.getInstance().getTimeInMillis()));
		fechaTxt=UtilString.fechaActual();
		horaSys=UtilString.horaActual();
		
		String htmlTemplate = Util.obtenerPlantillaCorreo("tmpl-notificacion-pago");
		htmlTemplate = Util.reemplazaSeccionPlantilla(htmlTemplate, "${nroTarjeta}", transferencia.getNroTarjeta());
		htmlTemplate = Util.reemplazaSeccionPlantilla(htmlTemplate, "${titular}", datosTitular.getTitular());
		htmlTemplate = Util.reemplazaSeccionPlantilla(htmlTemplate, "${fecEmi}", fechaTxt + " " + horaSys);
		htmlTemplate = Util.reemplazaSeccionPlantilla(htmlTemplate, "${nroOp}", nroOperacion.toString());
		htmlTemplate = Util.reemplazaSeccionPlantilla(htmlTemplate, "${ctaOrigen}", transferencia.getNroCuentaOrigen());
		htmlTemplate = Util.reemplazaSeccionPlantilla(htmlTemplate, "${ctaDestino}", transferencia.getNroCuentaDestino());
		htmlTemplate = Util.reemplazaSeccionPlantilla(htmlTemplate, "${monto}", String.valueOf(transferencia.getMonto()));
		try {
			Mensaje mensaje=new Mensaje();
			CuentaMensaje cuentaDestino=new CuentaMensaje();
			cuentaDestino.setEmail(correoDestino);
			mensaje.setCuentaDestino(cuentaDestino);
			mensaje.setMensajeAsunto("CONSTANCIA DE TRANSFERENCIA");
			mensaje.setMensajeContenido(htmlTemplate);
			mensaje.setConfigSiHtml(true);
			mensaje.setAplicacionEnvio("Servicio Datos");
			RestTemplate restTemplate=new RestTemplate();
			String uriMensajeria="http://10.0.7.105:8092/api/mensajeria/envia-email";
			restTemplate.postForObject(uriMensajeria, mensaje, HashMap.class);
		}catch(Exception ex) {
			System.out.println("ERROR ENVIO MENSAJE: "+ex.getMessage());
		}
		
		
	}
	
	
}
