package com.servicio.datos.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.AhorrosVO;
import com.servicio.datos.model.listaAhorrosResponse;
import com.servicio.datos.service.AhorrosService;
import com.servicio.datos.service.TokenService;


@RestController
public class AhorroController {

	@Autowired
	private AhorrosService ahorrosService;
	
	@Autowired
	private TokenService tokenService;
	
	@PostMapping("/ahorros")
	public listaAhorrosResponse obtenerAhorro(@RequestBody listaAhorrosResponse ahorro ) throws DAOException{
		
		listaAhorrosResponse ahorroR=new listaAhorrosResponse();
		List<AhorrosVO> ahorros = new ArrayList<AhorrosVO>();
		
		boolean esValido = false;
		
		if(ahorro.getCodigo() == null || ahorro.getToken() == null || ahorro.getCodigo().isEmpty() || ahorro.getToken().isEmpty()){
			ahorroR.setMensaje("Parametros incorrectos");
			ahorroR.setCodigo("JSR-101");
			ahorroR.setSuccess(false);
			return ahorroR;
		}
		
		

		try{
			
			esValido = tokenService.buscarTokenValido(ahorro.getToken());
			
		}catch (Exception e){
			
			ahorroR.setMensaje("Por favor comunicarse con Soporte y notificar codigo de error");
			ahorroR.setCodigo("JSR-201");
			ahorroR.setSuccess(false);
			return ahorroR;
			
		}
		
		if(esValido == true){
			
		ahorros = ahorrosService.ListarCtas(ahorro.getCodigo());
		ahorroR.setCuentas(ahorros);
		ahorroR.setSuccess(true);
		
		return ahorroR;
		
		} else {
			
			
			ahorroR.setMensaje("No Autenticado");
			ahorroR.setCodigo("JSR-105");
			ahorroR.setSuccess(false);
			
			return ahorroR;
		}
		
		
	
	}
}
