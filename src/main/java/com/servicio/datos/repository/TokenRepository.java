package com.servicio.datos.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import com.servicio.datos.exception.DAOException;

@Repository
public class TokenRepository {
	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate dao;
	
	public boolean buscarTokenValido(String token) throws DAOException {
		String busqueda = "SELECT KEY FROM TBL_TOKEN WHERE KEY=?"
				+ " AND TIMESTAMP >= (CURRENT_TIMESTAMP - NUMTODSINTERVAL(5,'MINUTE'))";
		
		boolean respuesta=false;
		try{
			System.out.println("Ingresó a validar token");
			
			DataSource dataS=dao.getDataSource();
			
			Connection ora = dataS.getConnection();
			
			PreparedStatement stmt = ora.prepareStatement(busqueda);
			
			stmt.setString(1, token);
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next()){
				
				respuesta=true;
				
			}
			System.out.println("finalizó a validar token");
			stmt.close();
			ora.close();
			rs.close();
			} catch (SQLException e){
				e.printStackTrace();
				throw new DAOException(e);
			
			}
			return respuesta;
		}
	
	
	public void registrarToken(String token) throws DAOException{
		String insert = "INSERT INTO TBL_TOKEN ("+
				   "ID_TOKEN, KEY, TIMESTAMP)"+ 
				   " VALUES (ID_TOKEN.NEXTVAL,?,SYSTIMESTAMP)";
		
		try{
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();			
			PreparedStatement stmt = ora.prepareStatement(insert);
			
			stmt.setString(1, token);
			
			int i = stmt.executeUpdate();
			
			if( i != 1){
				
				throw new SQLException("No se pudo insertar registro");
				
			}
			
			stmt.close();
			
			ora.close();
			
		} catch (SQLException e){
			
			throw new DAOException(e);
			
		}
	}
}

