package com.servicio.datos.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.AhorrosVO;
import com.servicio.datos.model.TransferenciaVO;

import oracle.jdbc.OracleTypes;

@Repository
public class TransferenciaRepository {

	
	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate dao;
	
	public TransferenciaVO TransferenciaEntreCuentas(TransferenciaVO transferencia) throws DAOException {
	
	    TransferenciaVO trasnferenciaR=new TransferenciaVO();
		String callprocedure = "{call transfiereAhorrosMismoSocio(?,?,?,?,?,?,?)}";
		
		
		try{
			System.out.println("inicio transferencia");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			stmt.setInt(1,transferencia.getCtaOrigen());
			stmt.setInt(2,transferencia.getCtaDestino());
			stmt.setDouble(3,transferencia.getMonto());
			stmt.setString(4,transferencia.getCanal());
			stmt.registerOutParameter(5,OracleTypes.VARCHAR);
			stmt.registerOutParameter(6,OracleTypes.VARCHAR);
			stmt.registerOutParameter(7,OracleTypes.INTEGER);
			stmt.executeUpdate();
			try {
				transferencia.setRespuesta(stmt.getObject(5).toString());
				if(stmt.getObject(6)!=null) {
					transferencia.setDetalleRespuesta(stmt.getObject(6).toString());	
				}else {
					transferencia.setIdTransaccion(Integer.parseInt(stmt.getObject(7).toString()));	
				}
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				stmt.close();
				ora.close();
				throw ex;
			}	
			System.out.println("Finalizó transferencia");
		
		}catch (SQLException e){
			throw new DAOException(e);
		}
		
		return transferencia;
	}
	
	public TransferenciaVO DatosTitular(TransferenciaVO transferencia) throws DAOException {
		
	    TransferenciaVO trasnferenciaR=new TransferenciaVO();
		String callprocedure = "{call getDatosSocioCursor(?,?)}";
		
		
		try{
			System.out.println("inicio datos transferencia con codigo:" + transferencia.getCodigo());
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			stmt.setString(1,transferencia.getCodigo());
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(2);
			try {
				while(rs.next()){
					
					
					transferencia.setTitular(rs.getString("NOMBRE"));
					transferencia.setCorreo(rs.getString("EMAIL"));
//					ah.setMoneda(rs.getString("moneda"));
//					ah.setDescripcion(rs.getString("categprod"));
//					ah.setSaldo(rs.getDouble("mtocap"));
//					ah.setTipoCuenta(rs.getString("TIPOCUENTA"));
//					ah.setNroCuenta(rs.getString("NROCTA"));
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				throw ex;
			}	
			
			System.out.println("Finalizó listar cuentas destino");	
			System.out.println("Finalizó transferencia");
		
		}catch (SQLException e){
			throw new DAOException(e);
		}
		
		return transferencia;
	}
	
}
