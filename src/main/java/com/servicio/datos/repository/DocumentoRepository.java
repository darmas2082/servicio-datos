package com.servicio.datos.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import oracle.jdbc.OracleTypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.DocumentosVO;
import com.servicio.datos.model.Prestamos;

@Repository
public class DocumentoRepository {
	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate dao;
	
public List<Prestamos> ListarDocumentos(String Codigo) throws DAOException{
		
	List<Prestamos> c = new ArrayList<Prestamos>();
		
		String query="{call GETFPSCURSOR(?,?)}";
		
		try{
			System.out.println("Ingresó a listar docs");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(query);
			int code = Integer.parseInt(Codigo);
			stmt.setInt(1,code);
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();
			System.out.println(Codigo);
			ResultSet rs = (ResultSet)stmt.getObject(2);
			try {
				while(rs.next()){
					Prestamos doc = new Prestamos();
					doc.setFecVigencia(rs.getDate("FECVIGENCIA"));
					doc.setFecVencimiento(rs.getDate("FECVENCIMIENTO"));
					doc.setCondicion(rs.getString("CONDICION"));
					c.add(doc);
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				throw ex;
			}
			System.out.println("Finalizó listar docs");
			
			
			
		}catch (SQLException e){
			
			throw new DAOException(e);
			
		}
		
		return c;
		
	}
}
