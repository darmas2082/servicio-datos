package com.servicio.datos.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import oracle.jdbc.OracleTypes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.exception.LoginException;
import com.servicio.datos.model.SocioVO;

@Repository
public class SocioRepository {

	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate dao;
	
	public SocioVO registrarClaveWeb(String nroTarjeta, String pin) throws DAOException{
		
		SocioVO vo = new SocioVO();
		String sqlacceso = "{call setAccesoCursor(?,?,?)}";
		
		try {
			long tarjeta = Long.parseLong(nroTarjeta);
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(sqlacceso);
			stmt.setLong(1, tarjeta);
			stmt.setInt(2, Integer.parseInt(pin));
			stmt.registerOutParameter(3,OracleTypes.CURSOR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(3);
			try {
				if(rs.next()) {
					vo.setNombres(rs.getString("nombre"));
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				ex.printStackTrace();	
			}
		} catch(SQLException e){
			e.printStackTrace();
		}
		
		return vo;
	
	}
	
	public SocioVO validarPIN(String nroTarjeta,Integer pin) throws LoginException{
		
		SocioVO vo =null;
		
		String sqlconsulta = "{call getAutorizacionCursor(?,?,?,?,?)}";
		
		try {
			System.out.println("Ingresó a validar pin");
			long tarjeta = Long.parseLong(nroTarjeta);
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();
			String respuesta="";
			String detalleRespuesta="";
			CallableStatement stmt = ora.prepareCall(sqlconsulta);
			stmt.setLong(1, tarjeta);
			stmt.setInt(2, pin);
			stmt.registerOutParameter(3,OracleTypes.CURSOR);
			stmt.registerOutParameter(4,OracleTypes.VARCHAR);
			stmt.registerOutParameter(5,OracleTypes.VARCHAR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(3);
			try {
				respuesta=stmt.getObject(4).toString();
				detalleRespuesta=stmt.getObject(5).toString();
				if(rs.next()) {
					vo = new SocioVO();
					vo.setSuccess(true);
					vo.setNombres(rs.getString("nombre"));
					rs.close();
					stmt.close();
					ora.close();				
				} else {
					vo = new SocioVO();
					vo.setSuccess(false);
					vo.setMensajeDetallado(detalleRespuesta);
					rs.close();
					stmt.close();
					ora.close();
//					throw new LoginException();
					
				}	
				System.out.println("finalizó a validar pin");
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				ex.printStackTrace();	
			}
			
				
			
			
		} catch(SQLException e){
			
			e.printStackTrace();
			
		}
		
		return vo;
	}
	public SocioVO validarTarjeta(String nroTarjeta, String pin) {
		SocioVO vo = new SocioVO();
		String sqlquery = "{call getAccesoCursor(?,?,?)}";
		try{
			System.out.println("Ingresó a validar tarjeta");
			long tarjeta = Long.parseLong(nroTarjeta);
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();
			CallableStatement stmt = ora.prepareCall(sqlquery);
			stmt.setLong(1, tarjeta);
			stmt.setInt(2, Integer.parseInt(pin));
			stmt.registerOutParameter(3, OracleTypes.CURSOR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(3);
			if(rs.next()){
				
				vo.setCodigo(rs.getString("IDTERCERO"));
				vo.setNombres(rs.getString("NOMBRE"));
				
			} else {
				
				vo = null;
				
			}
			System.out.println("Finalizó a validar token");
			rs.close();
			stmt.close();
//			con.commit();
			ora.close();
		} catch (SQLException e){
			
			e.printStackTrace();
			
		}
		
		return vo;
	}
	
}
