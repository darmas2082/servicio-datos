package com.servicio.datos.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.MovimientoPrestamoVO;
import com.servicio.datos.model.PrestamosVO;
import com.servicio.datos.model.TransferenciaVO;

import oracle.jdbc.OracleTypes;

@Repository
public class PrestamosRepository {

	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate dao;
	
	public List<MovimientoPrestamoVO> ListarCuotasPendientes(String id_prestamo)throws DAOException{
		List<MovimientoPrestamoVO> c = new ArrayList<MovimientoPrestamoVO>();
		
		String consulta="{call GETPRESTAMOSDETALLEPENDICURSOR(?,?)}";
		
		try{
			
			DataSource dataS=dao.getDataSource();
			Connection con = dataS.getConnection();
			CallableStatement stmt = con.prepareCall(consulta);
			int idPrestamo = Integer.parseInt(id_prestamo);
			stmt.setInt(1,idPrestamo);
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();

			ResultSet rs = (ResultSet)stmt.getObject(2);
			
			while(rs.next()){
				
				MovimientoPrestamoVO cr = new MovimientoPrestamoVO();
				
				cr.setFecha(rs.getDate("FECVENC"));
				cr.setCuota(rs.getInt("NROCUOTA"));
				cr.setCapital(rs.getFloat("SLDOCAP"));
				cr.setInteres(rs.getFloat("SLDOINTERES"));
				cr.setCargo(rs.getFloat("SLDOCARGO"));
				cr.setMora(rs.getFloat("SLDOMORA"));
				cr.setTotal(rs.getFloat("MTOCUOTA"));
				
				c.add(cr);
				
			}
			
			rs.close();
			stmt.close();
			con.close();
			
		}catch (SQLException e){
			
			e.printStackTrace();
			throw new DAOException(e);
			
		}
		
		return c;
	}
	public List<MovimientoPrestamoVO> ListarCuotasPagadas(String id_prestamo)throws DAOException{
		List<MovimientoPrestamoVO> c = new ArrayList<MovimientoPrestamoVO>();
		
		String consulta="{call GETPRESTAMOSDETALLEPAGOSCURSOR(?,?)}";
		
		try{
			
			DataSource dataS=dao.getDataSource();
			Connection con = dataS.getConnection();
			CallableStatement stmt = con.prepareCall(consulta);
			int idPrestamo = Integer.parseInt(id_prestamo);
			stmt.setInt(1,idPrestamo);
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();

			ResultSet rs = (ResultSet)stmt.getObject(2);
			
			while(rs.next()){
				
				MovimientoPrestamoVO pr = new MovimientoPrestamoVO();
				
				
				
				pr.setFecha(rs.getDate("FECTRX"));
				pr.setCuota(rs.getInt("CUOTA"));
				pr.setCapital(rs.getFloat("MTOCAPCAJA"));
				pr.setInteres(rs.getFloat("MTOINTCAJA"));
				pr.setCargo(rs.getFloat("MTOCARGO"));
				pr.setMora(rs.getFloat("MTORECA"));
				pr.setTotal(rs.getFloat("TOTAL"));
				
				c.add(pr);
				
			}
			
			rs.close();
			stmt.close();
			con.close();
			
		}catch (SQLException e){
			
			e.printStackTrace();
			throw new DAOException(e);
			
		}
		
		return c;
	}
	public List<PrestamosVO> ListarPrestamos(String Codigo) throws DAOException{
		List<PrestamosVO> c = new ArrayList<PrestamosVO>();
		
		String callprocedure = "{call GETPRESTAMOSCURSOR(?,?)}";
		
		try{
			System.out.println("Ingresó a listar prestamos");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			int code = Integer.parseInt(Codigo);
			stmt.setInt(1,code);
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(2);
			try {
				while(rs.next()){
					
					PrestamosVO pr = new PrestamosVO();
					pr.setId_prestamo(rs.getString("IDPRESTAMO"));
					pr.setProducto(rs.getString("PRODUCTO"));
					pr.setMoneda(rs.getString("MONEDA"));
					pr.setMonto(rs.getDouble("MTOPRESTAMO"));
					pr.setInteres(rs.getDouble("INTERES"));
					pr.setMora(rs.getDouble("MORA"));
					pr.setCapital(rs.getDouble("CAPITAL"));
					pr.setNroPrestamos(rs.getString("NROPRES"));
					//pr.setEstado(rs.getString("ESTADO"));
					c.add(pr);
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				throw ex;
			}
			System.out.println("Finalizó a listar prestamos");
			
		}catch (SQLException e){
			
			throw new DAOException(e);
			
		}
		return c;
	}
	
	public List<PrestamosVO>  ListaPrestamosDestino(PrestamosVO prestamo) throws DAOException {
		List<PrestamosVO> c = new ArrayList<PrestamosVO>();
		String callprocedure = "{call getPrestamosDestinoCursor(?,?)}";
		System.out.println("Ingresó a listar prestamos");
		try {
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			int code = Integer.parseInt(prestamo.getCodigo());
			stmt.setInt(1,code);
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(2);
			try {
				while(rs.next()){
					
					PrestamosVO pr = new PrestamosVO();
					pr.setId_prestamo(rs.getString("IDPRESTAMO"));
					pr.setNroPrestamos(rs.getString("NROPRES"));
					pr.setMoneda(rs.getString("MONEDA"));
					pr.setProducto(rs.getString("PRODUCTO"));
					//pr.setCuotaPendiente(rs.getInt("CUOTA"));
					pr.setFechaVencimiento(rs.getDate("FECVENCUOTA"));
					pr.setMonto(rs.getDouble("CUOTA"));
//					pr.setInteres(rs.getDouble("INTERES"));
//					pr.setMora(rs.getDouble("MORA"));
//					pr.setCapital(rs.getDouble("CAPITAL"));
//					
					//pr.setEstado(rs.getString("ESTADO"));
					c.add(pr);
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				throw ex;
			}
			System.out.println("Finalizó a listar prestamos");
			
		}catch (SQLException e){
			
			throw new DAOException(e);
			
		}
		return c;
	}
	public TransferenciaVO PagoPrestamo(TransferenciaVO transferencia) throws DAOException {
		
//		CTAORIGEN NUMBER,
//	    CTADESTINO  NUMBER,
//	    MONTO NUMBER,
//	    CANAL VARCHAR2, 
//	    RPTA OUT VARCHAR2,
//	    DET_RPTA OUT VARCHAR2,
//	    IDTRX OUT NUMBER
	    TransferenciaVO trasnferenciaR=new TransferenciaVO();
		String callprocedure = "{call pagaPrestamos(?,?,?,?,?,?)}";
		
		
		try{
			System.out.println("inicio pago préstamo");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			stmt.setInt(1,transferencia.getCtaOrigen());
			stmt.setInt(2,transferencia.getPrestamoDestino());
			stmt.setString(3,transferencia.getCanal());
			stmt.registerOutParameter(4,OracleTypes.VARCHAR);
			stmt.registerOutParameter(5,OracleTypes.VARCHAR);
			stmt.registerOutParameter(6,OracleTypes.INTEGER);
			stmt.executeUpdate();
			try {
				transferencia.setRespuesta(stmt.getObject(4).toString());
				if(stmt.getObject(5)!=null) {
					transferencia.setDetalleRespuesta(stmt.getObject(5).toString());	
				}else {
					transferencia.setIdTransaccion(Integer.parseInt(stmt.getObject(6).toString()));	
				}
			
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				stmt.close();
				ora.close();
				throw ex;
			}	
			System.out.println("Finalizó pago préstamo");
		
		}catch (SQLException e){
			throw new DAOException(e);
		}
		
		return transferencia;
	}
	
	
}
