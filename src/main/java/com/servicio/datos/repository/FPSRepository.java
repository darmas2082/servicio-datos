package com.servicio.datos.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.PrestamosVO;
import com.servicio.datos.model.TransferenciaVO;

import oracle.jdbc.OracleTypes;

@Repository
public class FPSRepository {
	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate dao;
	
	public double FPSDeudaObtener(String codigo) throws Exception{
			PrestamosVO prestamo=new PrestamosVO();
			String busqueda = "SELECT getFPSDeuda("+codigo+")FROM DUAL";
			
			boolean respuesta=false;
			double deuda=dao.queryForObject(busqueda, Double.class);
			return deuda;
	}
	
	public TransferenciaVO PagoFPS(TransferenciaVO transferencia) throws DAOException {
		
	    TransferenciaVO trasnferenciaR=new TransferenciaVO();
		String callprocedure = "{call pagaFPS(?,?,?,?,?,?)}";
		
		try{
			System.out.println("inicio pago FPS");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			stmt.setInt(1,transferencia.getCtaOrigen());
			stmt.setInt(2,Integer.valueOf(transferencia.getCodigo()));
			stmt.setString(3,transferencia.getCanal());
			stmt.registerOutParameter(4,OracleTypes.VARCHAR);
			stmt.registerOutParameter(5,OracleTypes.VARCHAR);
			stmt.registerOutParameter(6,OracleTypes.INTEGER);
			stmt.executeUpdate();
			try {
				transferencia.setRespuesta(stmt.getObject(4).toString());
				if(stmt.getObject(5)!=null) {
					transferencia.setDetalleRespuesta(stmt.getObject(5).toString());	
				}else {
					transferencia.setIdTransaccion(Integer.parseInt(stmt.getObject(6).toString()));	
				}
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				stmt.close();
				ora.close();
				throw ex;
			}	
			System.out.println("Finalizó pago FPS");
		
		}catch (SQLException e){
			throw new DAOException(e);
		}
		
		return transferencia;
	}
	
}
