package com.servicio.datos.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.MovimientoVO;

import oracle.jdbc.OracleTypes;

@Repository
public class MovimientoVORepository {
	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate dao;
	
	public List<MovimientoVO> ListarMovAhorros(String idtercero,String id_ahorro) throws DAOException{
		List<MovimientoVO> c = new ArrayList<MovimientoVO>();
		
		String query="{call GETAHORROSDETALLECURSOR(?,?,?)}";
		
		try{
			System.out.println("Ingresó a listar mov");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();
			CallableStatement stmt = ora.prepareCall(query);
			int idTercero = Integer.parseInt(idtercero);
			int idAhorro = Integer.parseInt(id_ahorro);
			stmt.setInt(1,idTercero);
			stmt.setInt(2,idAhorro);
			stmt.registerOutParameter(3,OracleTypes.CURSOR);
			stmt.executeUpdate();

			ResultSet rs = (ResultSet)stmt.getObject(3);
			try {
				while(rs.next()){
					//
					MovimientoVO ah = new MovimientoVO();
					ah.setFecha(rs.getDate("FECDOC"));
					ah.setDescripcion(rs.getString("DESCRIPCION"));
					ah.setMonto(rs.getFloat("MTOCAPTRX"));
					c.add(ah);
					//
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				throw new DAOException(ex);	
			}
			System.out.println("Finalizó listar mov");
		}catch (SQLException e){
			
			throw new DAOException(e);
			
		}
		
		return c;
	}
}
