package com.servicio.datos.repository;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.servicio.datos.exception.DAOException;
import com.servicio.datos.model.AhorrosVO;

import oracle.jdbc.OracleTypes;

@Repository
public class AhorrosRepository {

	@Autowired
	@Qualifier("jdbcTemplate1")
	JdbcTemplate dao;
	
	public List<AhorrosVO> ListarCtas(String codigo) throws DAOException{
		List<AhorrosVO> c = new ArrayList<AhorrosVO>();
		
		String callprocedure = "{call GETAHORROSCURSOR(?,?)}";
		
		try{
			System.out.println("Ingresó a listar cuentas");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			int code = Integer.parseInt(codigo);
			stmt.setInt(1,code);
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(2);
			try {
				while(rs.next()){
					
					AhorrosVO ah = new AhorrosVO();
					ah.setIdAhorro(rs.getString("idahahorro"));
					ah.setMoneda(rs.getString("moneda"));
					ah.setDescripcion(rs.getString("categprod"));
					ah.setSaldo(rs.getDouble("mtocap"));
					ah.setTipoCuenta(rs.getString("TIPOCUENTA"));
					ah.setNroCuenta(rs.getString("NROCTA"));
					c.add(ah);
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				throw ex;
			}	
			
			System.out.println("Finalizó listar cuentas");
			
			
		}catch (SQLException e){
			throw new DAOException(e);
		}
		
		return c;
	}
	public List<AhorrosVO> ListarCtasDestino(String codigo) throws DAOException{
		List<AhorrosVO> c = new ArrayList<AhorrosVO>();
		
		String callprocedure = "{call getAhorrosCtaDestinoCursor(?,?)}";
		
		try{
			System.out.println("Ingresó a listar cuentas destino");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			int code = Integer.parseInt(codigo);
			stmt.setInt(1,code);
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(2);
			try {
				while(rs.next()){
					
					AhorrosVO ah = new AhorrosVO();
					ah.setIdAhorro(rs.getString("idahahorro"));
//					ah.setMoneda(rs.getString("moneda"));
//					ah.setDescripcion(rs.getString("categprod"));
//					ah.setSaldo(rs.getDouble("mtocap"));
//					ah.setTipoCuenta(rs.getString("TIPOCUENTA"));
//					ah.setNroCuenta(rs.getString("NROCTA"));
					c.add(ah);
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				throw ex;
			}	
			
			System.out.println("Finalizó listar cuentas destino");
			
			
		}catch (SQLException e){
			throw new DAOException(e);
		}
		
		return c;
	}
	public List<AhorrosVO> ListarCtasOrigen(String codigo) throws DAOException{
		List<AhorrosVO> c = new ArrayList<AhorrosVO>();
		
		String callprocedure = "{call getAhorrosCtaOrigenCursor(?,?)}";
		
		try{
			System.out.println("Ingresó a listar cuentas origen");
			DataSource dataS=dao.getDataSource();
			Connection ora = dataS.getConnection();	
			CallableStatement stmt = ora.prepareCall(callprocedure);
			int code = Integer.parseInt(codigo);
			stmt.setInt(1,code);
			stmt.registerOutParameter(2,OracleTypes.CURSOR);
			stmt.executeUpdate();
			ResultSet rs = (ResultSet)stmt.getObject(2);
			try {
				while(rs.next()){
					
					AhorrosVO ah = new AhorrosVO();
					ah.setIdAhorro(rs.getString("idahahorro"));
//					ah.setMoneda(rs.getString("moneda"));
//					ah.setDescripcion(rs.getString("categprod"));
//					ah.setSaldo(rs.getDouble("mtocap"));
//					ah.setTipoCuenta(rs.getString("TIPOCUENTA"));
//					ah.setNroCuenta(rs.getString("NROCTA"));
					c.add(ah);
				}
				rs.close();
				stmt.close();
				ora.close();
			}catch(Exception ex) {
				rs.close();
				stmt.close();
				ora.close();
				throw ex;
			}	
			
			System.out.println("Finalizó listar cuentas origen");
			
			
		}catch (SQLException e){
			throw new DAOException(e);
		}
		
		return c;
	}
}
