package com.servicio.datos.config;

import java.util.TimeZone;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
@RefreshScope
public class WebConfig {

	 
	
	// CONFIGURACIONES PARA ORACLE
		private static final String DBORACLE_BEAN = "dboracle";
		private static final String DBORACLE_BEAN_JDBC = "jdbcTemplate1";
		private static final String DBORACLE_CONFIG_PREFIX = "spring.datasource";

		@Value("${spring.datasource.url}")
		private String dboracle_url;

		@Value("${spring.datasource.username}")
		private String dboracle_username;

		@Value("${spring.datasource.password}")
		private String dboracle_password;

		@Value("${spring.datasource.driverClassName}")
		private String dboracle_driverClassName;

		@Bean(name = DBORACLE_BEAN)
		@ConfigurationProperties(prefix = DBORACLE_CONFIG_PREFIX)
		public DataSource dataSource1() {
			System.setProperty("java.security.egd", "file:///dev/urandom");
			TimeZone timeZone = TimeZone.getTimeZone("America/Lima");
			TimeZone.setDefault(timeZone);

			return DataSourceBuilder.create()
					.url(dboracle_url)
					.username(dboracle_username)
					.password(dboracle_password)
					.driverClassName(dboracle_driverClassName)
					.build();
		}

		@Bean(name = DBORACLE_BEAN_JDBC)
		public JdbcTemplate jdbcTemplate1(@Qualifier(DBORACLE_BEAN) DataSource ds) {
			return new JdbcTemplate(ds);
		}
}
