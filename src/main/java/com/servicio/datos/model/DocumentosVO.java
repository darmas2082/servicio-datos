package com.servicio.datos.model;

import java.sql.Date;

public class DocumentosVO {
	private Date fpago;
	private String descripcion;
	private double monto;
	private int voucher;
	
	public Date getFpago() {
		return fpago;
	}
	public void setFpago(Date fechacarga) {
		this.fpago = fechacarga;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public int getVoucher() {
		return voucher;
	}
	public void setVoucher(int voucher) {
		this.voucher = voucher;
	}
}
