package com.servicio.datos.model;

import java.sql.Date;

public class MovimientoPrestamoVO {
	private Date fecha;
	private int cuota;
	private float capital;
	private float interes;
	private float cargo;
	private float mora;
	private float total;
	
	/**
	 * @param fecha
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public Date getFecha() {
		return fecha;
	}
	
	public int getCuota() {
		return cuota;
	}
	
	public void setCuota(int cuota) {
		this.cuota = cuota;
	}
	
	public float getCapital() {
		return capital;
	}
	
	public void setCapital(float capital) {
		this.capital = capital;
	}
	
	public float getInteres() {
		return interes;
	}
	
	public void setInteres(float interes) {
		this.interes = interes;
	}
	
	public float getCargo() {
		return cargo;
	}
	
	public void setCargo(float cargo) {
		this.cargo = cargo;
	}
	
	public float getMora() {
		return mora;
	}
	
	public void setMora(float mora) {
		this.mora = mora;
	}
	
	public float getTotal() {
		return total;
	}
	
	public void setTotal(float total) {
		this.total = total;
	}
}
