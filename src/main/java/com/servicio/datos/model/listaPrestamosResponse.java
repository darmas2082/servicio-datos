package com.servicio.datos.model;

import java.util.List;

public class listaPrestamosResponse {

	public String codigo;
	public String mensaje;
	public List<PrestamosVO> prestamos;
	public boolean success;
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<PrestamosVO> getPrestamos() {
		return prestamos;
	}
	public void setPrestamos(List<PrestamosVO> prestamos) {
		this.prestamos = prestamos;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	
	
}
