package com.servicio.datos.model;

public class TransferenciaVO {
	private Integer ctaOrigen;
	private Integer ctaDestino;
	private double monto;
	private String canal;
	private String respuesta;
	private String detalleRespuesta;
	private boolean success;
	private String token;
	
	private Integer prestamoDestino;

	private String codigo;
	private String nroTarjeta;
	private String nroCuentaOrigen;
	private String nroCuentaDestino;
	private String titular;
	private String correo;
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTitular() {
		return titular;
	}
	public void setTitular(String titular) {
		this.titular = titular;
	}
	public String getNroTarjeta() {
		return nroTarjeta;
	}
	public void setNroTarjeta(String nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}
	public String getNroCuentaOrigen() {
		return nroCuentaOrigen;
	}
	public void setNroCuentaOrigen(String nroCuentaOrigen) {
		this.nroCuentaOrigen = nroCuentaOrigen;
	}
	public String getNroCuentaDestino() {
		return nroCuentaDestino;
	}
	public void setNroCuentaDestino(String nroCuentaDestino) {
		this.nroCuentaDestino = nroCuentaDestino;
	}
	
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public Integer getPrestamoDestino() {
		return prestamoDestino;
	}
	public void setPrestamoDestino(Integer prestamoDestino) {
		this.prestamoDestino = prestamoDestino;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getDetalleRespuesta() {
		return detalleRespuesta;
	}
	public void setDetalleRespuesta(String detalleRespuesta) {
		this.detalleRespuesta = detalleRespuesta;
	}
	private Integer idTransaccion;
	public Integer getCtaOrigen() {
		return ctaOrigen;
	}
	public void setCtaOrigen(Integer ctaOrigen) {
		this.ctaOrigen = ctaOrigen;
	}
	public Integer getCtaDestino() {
		return ctaDestino;
	}
	public void setCtaDestino(Integer ctaDestino) {
		this.ctaDestino = ctaDestino;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public String getCanal() {
		return canal;
	}
	public void setCanal(String canal) {
		this.canal = canal;
	}
	public String getRespuesta() {
		return respuesta;
	}
	public void setRespuesta(String respuesta) {
		this.respuesta = respuesta;
	}
	public Integer getIdTransaccion() {
		return idTransaccion;
	}
	public void setIdTransaccion(Integer idTransaccion) {
		this.idTransaccion = idTransaccion;
	}
	
	
	
}
