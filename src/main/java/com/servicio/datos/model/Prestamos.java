package com.servicio.datos.model;

import java.sql.Date;

public class Prestamos {
	public String idTercero;
	public String token;
	public Date Fecha;
	public String Descripcion;
	public Double Monto;
	public int Voucher;
	
	
	public Date fecVigencia;
	public Date fecVencimiento;
	public String condicion;
	
	
	
	
	
	public Date getFecVigencia() {
		return fecVigencia;
	}
	public void setFecVigencia(Date fecVigencia) {
		this.fecVigencia = fecVigencia;
	}
	public Date getFecVencimiento() {
		return fecVencimiento;
	}
	public void setFecVencimiento(Date fecVencimiento) {
		this.fecVencimiento = fecVencimiento;
	}
	public String getCondicion() {
		return condicion;
	}
	public void setCondicion(String condicion) {
		this.condicion = condicion;
	}
	public String getIdTercero() {
		return idTercero;
	}
	public void setIdTercero(String idTercero) {
		this.idTercero = idTercero;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	public Date getFecha() {
		return Fecha;
	}
	public void setFecha(Date fecha) {
		Fecha = fecha;
	}
	public String getDescripcion() {
		return Descripcion;
	}
	public void setDescripcion(String descripcion) {
		Descripcion = descripcion;
	}
	public Double getMonto() {
		return Monto;
	}
	public void setMonto(Double monto) {
		Monto = monto;
	}
	public int getVoucher() {
		return Voucher;
	}
	public void setVoucher(int voucher) {
		Voucher = voucher;
	}
}
