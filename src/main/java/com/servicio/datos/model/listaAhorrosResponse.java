package com.servicio.datos.model;

import java.util.List;

public class listaAhorrosResponse {
	public String mensaje;
	public boolean success;
	public List<AhorrosVO> cuentas;
	
	public String codigo;
	public String token;
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public List<AhorrosVO> getCuentas() {
		return cuentas;
	}
	public void setCuentas(List<AhorrosVO> cuentas) {
		this.cuentas = cuentas;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	
	
}
