package com.servicio.datos.model;

import java.sql.Date;
import java.util.List;

public class PrestamosVO {

	private String id_prestamo;
	private String producto;
	private String moneda;
	private double monto;
	private double saldo;
	private double interes;
	private double mora;
	
	private double capital;
	private String nroPrestamos;
	
	private String token;
	
	
	private List<MovimientoPrestamoVO> cuotasPendiente;
	private List<MovimientoPrestamoVO> cuotasPagadas;
	
	private String mensaje;
	private boolean success;
	private String codigo;
	
	private Date fechaVencimiento;
	private Integer cuotaPendiente;
	
	public Date getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(Date fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	public Integer getCuotaPendiente() {
		return cuotaPendiente;
	}
	public void setCuotaPendiente(Integer cuotaPendiente) {
		this.cuotaPendiente = cuotaPendiente;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public boolean getSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public List<MovimientoPrestamoVO> getCuotasPendiente() {
		return cuotasPendiente;
	}
	public void setCuotasPendiente(List<MovimientoPrestamoVO> cuotasPendiente) {
		this.cuotasPendiente = cuotasPendiente;
	}
	public List<MovimientoPrestamoVO> getCuotasPagadas() {
		return cuotasPagadas;
	}
	public void setCuotasPagadas(List<MovimientoPrestamoVO> cuotasPagadas) {
		this.cuotasPagadas = cuotasPagadas;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public double getCapital() {
		return capital;
	}
	public void setCapital(double capital) {
		this.capital = capital;
	}
	public String getNroPrestamos() {
		return nroPrestamos;
	}
	public void setNroPrestamos(String nroPrestamos) {
		this.nroPrestamos = nroPrestamos;
	}
	public String getId_prestamo() {
		return id_prestamo;
	}
	public void setId_prestamo(String id_prestamo) {
		this.id_prestamo = id_prestamo;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public double getSaldo() {
		return saldo;
	}
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getInteres() {
		return interes;
	}
	public void setInteres(double interes) {
		this.interes = interes;
	}
	public double getMora() {
		return mora;
	}
	public void setMora(double mora) {
		this.mora = mora;
	}
	
	
}
