package com.servicio.datos.model;

import java.util.List;

public class MovimientoResponse {

	public String codigo;
	public String mensaje;
	public List<MovimientoVO> prestamos;
	public List<MovimientoVO> ahorros;
	public boolean success;
	
	public List<MovimientoVO> getAhorros() {
		return ahorros;
	}
	public void setAhorros(List<MovimientoVO> ahorros) {
		this.ahorros = ahorros;
	}
	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<MovimientoVO> getPrestamos() {
		return prestamos;
	}
	public void setPrestamos(List<MovimientoVO> prestamos) {
		this.prestamos = prestamos;
	}
	
	
}
