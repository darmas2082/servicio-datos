package com.servicio.datos.model;

public class SocioVO {

	private String nroTarjeta;
	private String pin;
	private String codigo;
	private String nombres;
	private boolean success;
	private String mensaje;
	
	private String token;
	
	
	private String mensajeDetallado;
	
	
	
	
	
	public String getMensajeDetallado() {
		return mensajeDetallado;
	}

	public void setMensajeDetallado(String mensajeDetallado) {
		this.mensajeDetallado = mensajeDetallado;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getNroTarjeta() {
		return nroTarjeta;
	}

	public void setNroTarjeta(String nroTarjeta) {
		this.nroTarjeta = nroTarjeta;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	
	public String getNombres() {
		return nombres;
	}
}
