package com.servicio.datos.model;

import java.util.List;

public class listaFPSResponse {
	public String codigo;
	public String mensaje;
	public List<Prestamos> prestamos;
	public Boolean success;
	
	public Boolean getSuccess() {
		return success;
	}
	public void setSuccess(Boolean success) {
		this.success = success;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public List<Prestamos> getPrestamos() {
		return prestamos;
	}
	public void setPrestamos(List<Prestamos> prestamos) {
		this.prestamos = prestamos;
	}
}
