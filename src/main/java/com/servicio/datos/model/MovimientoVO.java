package com.servicio.datos.model;

import java.sql.Date;

public class MovimientoVO {
	private String idTercero;
	private String idAhorro;
	private String token;
	
	
	private Date fecha;
	private String agencia;
	private String descripcion;
	private float monto;
	
	
	
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getIdTercero() {
		return idTercero;
	}
	public void setIdTercero(String idTercero) {
		this.idTercero = idTercero;
	}
	public String getIdAhorro() {
		return idAhorro;
	}
	public void setIdAhorro(String idAhorro) {
		this.idAhorro = idAhorro;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getAgencia() {
		return agencia;
	}
	public void setAgencia(String agencia) {
		this.agencia = agencia;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public float getMonto() {
		return monto;
	}
	public void setMonto(float monto) {
		this.monto = monto;
	}

}
