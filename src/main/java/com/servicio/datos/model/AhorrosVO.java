package com.servicio.datos.model;

public class AhorrosVO {
	private String idAhorro;
	private String moneda;
	private String descripcion;
	private double saldo;
	private String tipoCuenta;
	private String nroCuenta;
	
	private boolean esOrigen;
	private double saldoDiarioTransferencia;
	private boolean esDestino;
	//
	
	
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public boolean isEsOrigen() {
		return esOrigen;
	}
	public void setEsOrigen(boolean esOrigen) {
		this.esOrigen = esOrigen;
	}
	public double getSaldoDiarioTransferencia() {
		return saldoDiarioTransferencia;
	}
	public void setSaldoDiarioTransferencia(double saldoDiarioTransferencia) {
		this.saldoDiarioTransferencia = saldoDiarioTransferencia;
	}
	public boolean isEsDestino() {
		return esDestino;
	}
	public void setEsDestino(boolean esDestino) {
		this.esDestino = esDestino;
	}
	public String getTipoCuenta() {
		return tipoCuenta;
	}
	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}
	public String getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(String nroCuenta) {
		this.nroCuenta = nroCuenta;
	}
	public String getIdAhorro() {
		return idAhorro;
	}
	public void setIdAhorro(String idAhorro) {
		this.idAhorro = idAhorro;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getDescripcion() {
		return descripcion;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	public double getSaldo() {
		return saldo;
	}
}
